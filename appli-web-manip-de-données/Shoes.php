<?php
/*
 * On indique que les chemins des fichiers qu'on inclut
 * seront relatifs au répertoire src.
 */
set_include_path("./src");

/* Inclusion des classes utilisées dans ce fichier */
require_once("Router.php");

require_once("model/ShoeStorage.php");
require_once("model/ConnexionPDO.php");
require_once("model/ShoeStorageMySQL.php");
require_once("control/AuthenticationManager.php");
require_once("control/Controller.php");
require_once("view/View.php");
require_once("view/PrivateView.php");

require_once("model/Shoe.php");
require_once("model/ShoeBuilder.php");
require_once("model/Account.php");
require_once("model/AccountBuilder.php");
require_once("model/AccountStorage.php");
require_once("model/AccountStorageMySQL.php");


/*
 * Cette page est simplement le point d'arrivée de l'internaute
 * sur notre site. On se contente de créer un routeur
 * et de lancer son main.
 */
$router = new Router();

$aShoeStorage = new ShoeStorageMySQL($bd);
$anAccountStorageMySQL = new AccountStorageMySQL($bd);

$router->main($aShoeStorage, $anAccountStorageMySQL);
