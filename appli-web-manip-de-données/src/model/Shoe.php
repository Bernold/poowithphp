<?php
	class Shoe{

	protected $libelle;
	protected $marque;
	protected $modele;
	protected $couleur;
	protected $prix;
	protected $pointure;

	public function __construct($newLibelle, $newMarque, $newModele, $newCouleur,$newPrix, $newPointure){
		$this->libelle = $newLibelle;
		$this->marque = $newMarque;
		$this->modele = $newModele;
		$this->couleur = $newCouleur;
		$this->prix = $newPrix;
		$this->pointure = $newPointure;
	}

	public function getWording(){
		return $this->libelle;
	}

	public function getBrand(){
		return $this->marque;
	}

	public function getModel(){
		return $this->modele;
	}

	public function getColor(){
		return $this->couleur;
	}

	public function getPrice(){
		return  $this->prix ;
	}

	public function getPriceWithdevise(){
		return  $this->prix . " €";
	}

	public function getShoeSize(){
		return $this->pointure;
	}


	public function setWording($newLibelle){
			$this->libelle = $newLibelle;
		}

	public function setBrand($newMarque){
			$this->marque = $newMarque;
		}

	public function setModel($newModele){
			$this->modele = $newModele;
		}

	public function setColor($newCouleur){
			$this->couleur = $newCouleur;
		}

	public function setPrice($newPrix){
			$this->prix = $newPrix;
		}

	public function setPointure($newPointure){
			$this->pointure = $newPointure;
		}

	

}

