<?php
	class AccountStorageMySQL implements AccountStorage {


		public $base;

		public function __construct(PDO $abase)
		{
			$this->base = $abase;
		}


		// Méthode auxiliaire pour récupérer le contenu de la table utilisateur
		public function getUtilisateurTableContent()
		{
			$requete='SELECT * FROM utilisateur';
			$stmt=$this->base->prepare($requete);
			$stmt->execute();
			$theRequestResult = $stmt->fetchAll(PDO::FETCH_ASSOC);

			return $theRequestResult;

		}

	
		//Verifie l'authenticité d'un utilisateur lors de sa connexion
		public function checkAuth($login, $password)
		{
			$accounts = $this->getUtilisateurTableContent();
			foreach ($accounts as $ligne) 
			{
				if($login == $ligne['login'] && password_verify($password,$ligne['password']))
				{
					return  new Account($ligne['login'],$ligne['password'],$ligne['name']);
				}
			}
			return null;
		}



		//Verifie si le login qui va être inséré existe déjà
		public function ifLoginAlreadyExists($login)
		{
			$accounts = $this->getUtilisateurTableContent();
			foreach($accounts as $ligne)
			{
				if($login == $ligne['login'])
				{
					return false;
				}
			}
			return true;
		}
		
		//Insère un nouvel utilisateur inscrit, dans la base
		public function createAUser_In_Table_utilisateur(Account $account)
		{

				$requete="INSERT INTO utilisateur(login,password,name)VALUES(:login,:password,:name)";
				$data = array(':login' => $account->getLOGIN(), ':password' => $account->getPASSWORD(), ':name' => $account->getNAME());
				$stmt = $this->base->prepare($requete);
				$stmt->execute($data);

				return $this->base->lastInsertId();	
		}

		//Renvoi l'id de l'utilisateur dont le pseudo est passé en argument
		public function getUserID($userPseudo)
		{

			$theRequest = 'SELECT idUtilisateur FROM utilisateur WHERE login = :login';
			$stmt=$this->base->prepare($theRequest);
			$data=array(':login'=>$userPseudo);
			$stmt->execute($data);
			$resultat=$stmt->fetchAll(PDO::FETCH_ASSOC);

			foreach ($resultat as $ligne) 
			{
				return $ligne['idUtilisateur'];
			}
		}


}