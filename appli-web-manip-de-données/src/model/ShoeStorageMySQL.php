<?php
	class ShoeStorageMySQL implements ShoeStorage {
		
		
		public $db;

		// Contruction de l'instance PDO dans le constructeur
		public function __construct(PDO $adb){
			$this->db = $adb;
		}


		// Lit une instance de chaussure grâce à son id
		public function read($idChaussure){
			$requete='SELECT * FROM chaussure WHERE idChaussure=:idChaussure';
			$stmt=$this->db->prepare($requete);
			$data=array(':idChaussure'=>$idChaussure);
			$stmt->execute($data);
			$resultat=$stmt->fetchAll(PDO::FETCH_ASSOC);

			foreach ($resultat as $ligne) {
				return new Shoe($ligne['libelle'],$ligne['marque'],$ligne['modele'],$ligne['couleur'],$ligne['prix'],$ligne['pointure']);  
			} 
		}


		// Renvoi le contenu de la table chaussure comme un tableau associatif
		public function readAll(){
			$requete='SELECT * FROM chaussure';
			$stmt=$this->db->prepare($requete);
			$stmt->execute();
			$resultat=$stmt->fetchAll(PDO::FETCH_ASSOC);

			$data=array();

			for($i=0;$i<count($resultat);$i++){
				$data[$resultat[$i]['idChaussure']]=new Shoe($resultat[$i]['libelle'],$resultat[$i]['marque'],$resultat[$i]['modele'],$resultat[$i]['couleur'],$resultat[$i]['prix'],$resultat[$i]['pointure']);
			}
			return $data;
		}
		
		//Récupère l'id du créateur de la chaussure, renvoi l'id de la nouvelle chaussure créée
		public function create(Shoe $theShoe, Account $creationAuthor){

			$requete = ("INSERT INTO chaussure(libelle,marque,modele,couleur,prix,pointure,idUtilisateur)
				VALUES(:libelle,:marque,:modele,:couleur,:prix,:pointure,(SELECT idUtilisateur FROM utilisateur WHERE login =:login))");

			$data = array(':libelle'=>$theShoe->getWording(),':marque'=>$theShoe->getBrand(),':modele'=>$theShoe->getModel(),':couleur'=>$theShoe->getColor(),':prix'=>$theShoe->getPrice(),':pointure'=>$theShoe->getShoeSize(),':login'=>$creationAuthor->getLOGIN());

			$stmt = $this->db->prepare($requete);
			$stmt->execute($data);


			return $this->db->lastInsertId();

		}

		// supprime une chaussure de la base
		public function delete($id){
			$request = 'DELETE FROM chaussure WHERE idChaussure=:idChaussure';
			$stmt = $this->db->prepare($request);
			$stmt->execute(array(':idChaussure'=>$id));
		}

		// Met à jour une chaussure dans la base
		public function storeUpdatedShoe($idChaussure, Shoe $theShoe){

			$stmt=$this->db->prepare('UPDATE chaussure SET libelle =:libelle, marque=:marque, modele=:modele, couleur=:couleur,prix=:prix, pointure=:pointure WHERE idChaussure='.$idChaussure);
			$stmt->execute(array('libelle'=>$theShoe->getWording(),'marque'=>$theShoe->getBrand(),'modele'=>$theShoe->getModel(),'couleur'=>$theShoe->getColor(),'prix'=>$theShoe->getPrice(),'pointure'=> $theShoe->getShoeSize()));

		}


		// Recherche une chaussure dans la base en se basant sur sa marque
		public function researchShoeByBrand($marque){
			
			$requete="SELECT * FROM chaussure WHERE marque LIKE '$marque%' ";
			$stmt=$this->db->prepare($requete);
			$data=array(':marque'=>$marque);
			$stmt->execute($data);
			$resultatRequete=$stmt->fetchAll(PDO::FETCH_ASSOC);

			$tab = array();
			for($i=0;$i<count($resultatRequete);$i++){

				$tab[$resultatRequete[$i]['idChaussure']]=new Shoe($resultatRequete[$i]['libelle'],$resultatRequete[$i]['marque'],$resultatRequete[$i]['modele'],$resultatRequete[$i]['couleur'],$resultatRequete[$i]['prix'],$resultatRequete[$i]['pointure']);
			}
			return $tab;
		}

		// Renvoi le nombre de chaussures recherchés retrouvés
		public function getShoesSearchNumber($marque) {
				$requete="SELECT COUNT(*) as nb FROM chaussure WHERE marque LIKE '$marque%' ";
				$stmt = $this->db->prepare($requete);
				$stmt->execute();
				
				$resultat=$stmt->fetchAll(PDO::FETCH_ASSOC);
	
				foreach ($resultat as $ligne) {
					return $ligne['nb'];
				}
			}


		// Renvoi l'identifiant du créateur d'une chaussure
		public function getShoeAuthorID($idChaussure){
			$theRequest = 'SELECT idUtilisateur FROM chaussure WHERE idChaussure = :idChaussure';
			$stmt=$this->db->prepare($theRequest);
			$data=array(':idChaussure'=>$idChaussure);
			
			$stmt->execute($data);

			$resultat=$stmt->fetchAll(PDO::FETCH_ASSOC);

			foreach ($resultat as $ligne) {
				return $ligne['idUtilisateur'];
			}
		}

		// Renvoi le nombre de chaussure dans la base
		public function getShoesNumber() {
			$requete = 'SELECT COUNT(*) as taille FROM chaussure';
			$stmt = $this->db->prepare($requete);
			$stmt->execute();
			
			$resultat=$stmt->fetchAll(PDO::FETCH_ASSOC);

			foreach ($resultat as $ligne) {
				return $ligne['taille'];
			}
		}

}
