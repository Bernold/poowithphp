<?php
	class ShoeBuilder{


		private $data;
		private $error;
		

		const WORDING_REF = 'LIBELLE';
		const BRAND_REF = 'MARQUE';
		const MODEL_REF = 'MODELE';
		const COLOR_REF = 'COULEUR';
		const PRICE_REF = 'PRIX';
		const SHOE_SIZE_REF = 'POINTURE';


		public function __construct($aDATA){
			$this->data = $aDATA;
			$this->error = null;
		}


		public function getData(){
			return $this->data;
		}

		public function getError(){
			return $this->error;
		}

		public function createShoe(){
			return new Shoe($this->data[self::WORDING_REF],$this->data[self::BRAND_REF],$this->data[self::MODEL_REF], $this->data[self::COLOR_REF], $this->data[self::PRICE_REF], $this->data[self::SHOE_SIZE_REF]);
		}

		public function isValid(){
			if($this->data[self::WORDING_REF]=='' && $this->data[self::BRAND_REF]=='' && $this->data[self::MODEL_REF]=='' && $this->data[self::COLOR_REF]=='' && $this->data[self::PRICE_REF]=='' && $this->data[self::SHOE_SIZE_REF]==''){

				$this->error = "Vos champs sont vides, vous devez les remplir";
				return false;

			}elseif($this->data[self::WORDING_REF]==''){
				$this->error = "Votre champ Libellé n'est pas valide";
				return false;
			}elseif($this->data[self::BRAND_REF]=='') {
				$this->error = "Votre champ Marque  n'est pas valide";
				return false;
			}elseif($this->data[self::MODEL_REF]=='') {
				$this->error = "Votre champ Modèle est vide , vous devez le remplir";
				return false;
			}elseif($this->data[self::COLOR_REF]=='') {
				$this->error = "Votre champ Couleur  n'est pas valide";
				return false;
			}elseif($this->data[self::PRICE_REF]=='') {
				$this->error = "Votre champ Prix  n'est pas valide";
				return false;

			}elseif(key_exists(self::PRICE_REF,$this->data)&&($this->data[self::PRICE_REF]<0)){
				$this->error = "La valeur de votre champ Prix n'est pas valide";
				return false;
			}elseif($this->data[self::SHOE_SIZE_REF]=='' || $this->data[self::SHOE_SIZE_REF]<10) {
				$this->error = "Votre champ Pointure n'est pas valide";
				return false;
			}else{
				return true;
			}
		}

		/* Renvoie une nouvelle instance de ShoeBuilder avec les données modidifiables de la chaussure passée en argument*/
		public static function buildFromShoe(Shoe $aSHOE){
			return new ShoeBuilder(array(
				self::WORDING_REF => $aSHOE->getWording(),
				self::BRAND_REF => $aSHOE->getBrand(),
				self::MODEL_REF => $aSHOE->getModel(),
				self::COLOR_REF => $aSHOE->getColor(),
				self::PRICE_REF => $aSHOE->getPrice(),
				self::SHOE_SIZE_REF => $aSHOE->getShoeSize()
			));
		}


		/* La méthode ci dessous met à jour une instance de la classe Shoe avec les données fournies */

		public function updateShoe(Shoe $aShoe){
			if(key_exists(self::WORDING_REF, $this->data))
				$aShoe->setWording($this->data[self::WORDING_REF]);
			if(key_exists(self::BRAND_REF, $this->data))
				$aShoe->setBrand($this->data[self::BRAND_REF]);
			if(key_exists(self::MODEL_REF, $this->data))
				$aShoe->setModel($this->data[self::MODEL_REF]);
			if(key_exists(self::COLOR_REF, $this->data))
				$aShoe->setColor($this->data[self::COLOR_REF]);
			if(key_exists(self::PRICE_REF, $this->data))
				$aShoe->setPrice($this->data[self::PRICE_REF]);
			if(key_exists(self::SHOE_SIZE_REF,$this->data))
				 $aShoe->setPointure($this->data[self::SHOE_SIZE_REF]);

		}


	
		
	}

