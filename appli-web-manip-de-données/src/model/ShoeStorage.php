<?php

	interface ShoeStorage {
		public function read($idChaussure);
		public function readAll();
		public function delete($idChaussure);
		public function create(Shoe $theShoe, Account $creationAuthor);
		
	}