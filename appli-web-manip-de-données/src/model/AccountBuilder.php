<?php
	class AccountBuilder 
	{
		private $data;
		private $error;

		
		const LOGIN_REF = 'LOGIN';
		const PASSWORD_REF = 'PASSWORD';
		const NAME_REF = 'NOM';

		public function __construct($aData){
			$this->data = $aData;
			$this->error = null;
		}

		public function getData(){
			return $this->data;
		}


		public function getError(){
			return $this->error;
		}


		public function createAccount(){

				return new Account($this->data[self::LOGIN_REF], password_hash($this->data[self::PASSWORD_REF],PASSWORD_BCRYPT),$this->data[self::NAME_REF]);
		}


		public function isValid()
		{
			if($this->data[self::LOGIN_REF]=='' && $this->data[self::PASSWORD_REF]=='' && $this->data[self::NAME_REF] =='')
			{

				$this->error = "Vos champs sont vides, vous devez les remplir";
				return false;

			}elseif($this->data[self::LOGIN_REF]=='')
			{

				$this->error = "Le champ Login n'est pas valide";
				return false;
			}elseif($this->data[self::PASSWORD_REF]=='')
			{

				$this->error = "Le champ Mot de passe n'est pas valide";
				return false;
			}elseif($this->data[self::NAME_REF] =='')
			{

				$this->error = "Le champ Nom n'est pas valide";
				return false;
			}else
			{
				return true;
			}
		}


		public function sameLoginErrorMessage()
		{
			$this->error="Oups, Ce login existe déjà. Veuillez en choisir un autre";
			return $this->error;
		}

	}

