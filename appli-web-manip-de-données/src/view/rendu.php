<!DOCTYPE html>
<html>
<head>
	
	<link rel="stylesheet" href="style/theStyle.css" />
	<title> <?php echo $this->title; ?></title>
 	<meta charset="UTF-8"/>
 	
</head>
<body>
	<nav class="menu">
		<ul>
			<?php 
			foreach ($this->displayMenu() as $text => $link) 
			{
				echo "<a href=\"$link\">$text</a>";
			}
			?>
		</ul>
			
	</nav>
	<main>

			<nav class="feedback">
				<?php echo $this->feedback;?>
			</nav>

			<nav class="theTITLE">
				<h1><?php echo $this->title ;?></h1>
			</nav>


			<nav class="theCONTENT">
				<?php echo $this->content;?>
			</nav>
	</main>
</body>
</html>











