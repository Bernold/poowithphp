<?php

	class View{

		protected $title;
		protected $content;
		protected $router;
		protected $menu;
		protected $feedback;
	

		public function __construct(Router $newRouter, $newFeedBack){
			$this->title=null;
			$this->content=null;
			$this->router=$newRouter;
			$this->feedback=$newFeedBack;
			$this->menu=null;
			
		}

		public function getFeedBack(){
			return $this->feedback;
		}


		public function makeUnknownShoePage(){
			$this->title="Erreur";
			$this->content="Chaussure déjà supprimée";
		}


		public function noPermissionforDeletion(){
			$this->title = "Désolé";
			$this->content = "Vous ne pouvez pas supprimer cette chaussure car vous n'en êtes pas l'auteur";
		}


		public function noPermissionforUpdate(){
			$this->title = "Désolé";
			$this->content = "Vous ne pouvez pas modifier cette chaussure car vous n'en êtes pas l'auteur";
		}



		public function render(){
			include('rendu.php');
		}

		public function displayMenu(){
			$this->menu = array
			(
				'Accueil' => $this->router->getHomePage(),
				'Chaussures' => $this->router->getAllShoesPage(),
				'Connexion' => $this->router->getConnectionPage(),
				'Inscription' => $this->router->getInscription(),
				'A propos' => $this->router->getInfoGroupe(),
			);
			return $this->menu;
		}


		public function displayShoeCreationSuccess($id){
			return $this->router->POSTredirect($this->router->getShoeURL($id), "<h3> Votre chaussure a été bien créée </h3>");
		}


		public function displayShoeCreationFailure(){
			return $this->router->POSTredirect($this->router->getShoeCreationURL(), "<h3> Erreur dans la création de votre chaussure </h3>");
		}


		public function displayShoeUpdatingSuccess($id){
			return $this->router->POSTredirect($this->router->getShoeURL($id), "<h3> Votre chaussure a été bien modifiée </h3>");
		}

		/*
		public function displayShoeUpdatingFailure($id){
			return $this->router->POSTredirect($this->router->getShoeUpdateSavedPage($id), "<h3> Modification non valide </h3>");
		}
		*/

		public function displaySuppressionSuccess(){
			return $this->router->POSTredirect($this->router->getAllShoesPage(), "<h3> La chaussure a été bien supprimée </h3>");
		}


		public function displayConnexionSucess(){
			return $this->router->POSTredirect($this->router->getHomePage(), "<h3> Connexion réussie </h3>");
		}


		public function displayConnexionFailure(){
			return $this->router->POSTredirect($this->router->getConnectionPage(), "<h3> Connexion échouée </h3>");
		}

		public function displayDeconnexionSucess(){
			return $this->router->POSTredirect($this->router->getHomePage(), "<h3> Déconnexion réussie </h3>");
		}

		public function displayCompteCreationSuccess($id){
			return $this->router->POSTredirect($this->router->getHomePage(), "<h3> Votre compte a été bien créé </h3>");
		}

		public function deconSucess(){
			return $this->router->POSTredirect($this->router->getHomePage(), "<h3> Déconnexion réussie </h3>");
		}


		
		public function makeHomePage(){
		
			$this->title ="Confectionnez vos chaussures ";
			$this->content ="Bienvenue sur votre site  ";
		}

		public function makeShoeListPage($listeChaussures)
		{
			$this->loadSearchForm();
			foreach($listeChaussures as $key => $value) {
				$this->content .='<a href='.$this->router->getShoeURL($key).'>'.$value->getWording().'</a>'.'<br/>';
			}
		}

		public function noShoeCReatedMessage()
		{
			$this->title = "Bonjour";
			$this->content = "Aucune chaussure n'a encore été créée pour l'instant";
		}

		public function noShoeFound(){
			$this->content = "Aucune chaussure de cette marque trouvée";
		}


		public function makeShoeListPageByResearch($listeChaussures){
		
			$this->loadSearchForm();
			foreach($listeChaussures as $key => $value) {
				$this->content .='<a href='.$this->router->getShoeURL($key).'>'.$value->getWording().'</a>'.'<br/>';
			}
		}
		

		public function makeAproposPage() {

			$this->title ="A PROPOS ";
			$contenu = "<div class='apropos'>";
			$contenu.= "Vous pourrez confectionner le type de chaussure que vous voulez.<br/>";
			$contenu.= "Faites votre précommande après confection. ";
			$contenu.= "</div>";
		

			$this->content = $contenu;
		}

		

		public function makeShoePage(Shoe $chaussure, $id){

			$libelleChaussure = $chaussure->getWording();
			$marqueChaussure = $chaussure->getBrand();
			$modeleChaussure = $chaussure->getModel();
			$couleurChaussure = $chaussure->getColor();
			$prixChaussure = $chaussure->getPriceWithdevise();
			$pointureChaussure = $chaussure->getShoeSize();
			

			$this->title = "La chaussure $libelleChaussure";
			$contenu = "";
			$contenu.= '<br/>';
			$contenu.= "Libellé : $libelleChaussure".'<br/>';
			$contenu.= "Marque : $marqueChaussure".'<br/>';
			$contenu.= "Modèle : $modeleChaussure".'<br/>';
			$contenu.= "Couleur : $couleurChaussure".'<br/>';
			$contenu.= "Prix : $prixChaussure ".'<br/>';
			$contenu.= "Pointure : $pointureChaussure".'<br/>';

			$contenu.='<ul>';
			$contenu.='<li><a href='.$this->router->getShoeAskDeletionURL($id).'> Supprimer cette chaussure? </a></li>';
			$contenu.='<li><a href='.$this->router->getShoeUpdatePage($id).'> Modifier cette chaussure? </a></li>';
			$contenu.='</ul>';

			$this->content = $contenu;
		}


		public function makeNotFoundDetails(){
			$this->title = 'Ouh la!';
			$this->content = "Vous devez d'abord vous inscrire et/ou vous connecter pour voir le détail des chaussures";
		}


		public function makeDebugPage($variable){
			$this->title ='Debug';
			$this->content = '<pre>'.htmlspecialchars(var_export($variable,true)).'</pre>';
		}


		public function makeShoeCreationPage(ShoeBuilder $choeBuilder){
			$this->title = "Formulaire de création d'une chaussure";
			$donneesPOST=$choeBuilder->getData();
			ob_start();
			include('formulaireChaussure.php');
			echo '<br/>';
			$form=ob_get_clean();
			$this->content=$form;
			$this->content.=$choeBuilder->getError();
		}


		public function makeShoeDeletionPage($id, Shoe $chaussure){
			$libelleChaussure = $chaussure->getWording();
			$this->title = " Suppression de la chaussure $libelleChaussure";
			ob_start();
			include('boutonConfirmation.php');
			$form=ob_get_clean();
			$this->content = "<p> La chaussure <strong><em> $libelleChaussure </em></strong> va être supprimée.</p>\n";
			$this->content.=$form;
		}


		public function makeShoeUpdatePage($id, ShoeBuilder $chaussureBuilder){
			$donneesPOST = $chaussureBuilder->getData();
			$this->title = "Modification d'une chaussure";
			ob_start();
			include('modificationChaussure.php');
			echo '<br/>';
			$form=ob_get_clean();
			$this->content=$form;
			$this->content.=$chaussureBuilder->getError();
		}


		public function makeLoginFormPage(){
			$this->title = "Page de Connexion";
			ob_start();
			include('pageConnexion.php');
			echo '<br/>';
			$form=ob_get_clean();
			$this->content=$form;
		}


		public function makeInscription(AccountBuilder $accountBuilder){
			//$donneesPOST=$accountBuilder->getData();
			$this->title="Page d'inscription";
			ob_start();
			include('pageInscription.php');
			echo'<br/>';
			$form=ob_get_clean();
			$this->content=$form;
			$this->content.=$accountBuilder->getError();
		}

		public function makeInscription_2(AccountBuilder $accountBuilder){
			$this->title="Page d'inscription";
			ob_start();
			include('pageInscription.php');
			echo'<br/>';
			$form=ob_get_clean();
			$this->content=$form;
			$this->content.=$accountBuilder->sameLoginErrorMessage();
		}

		public function loadSearchForm()
		{
			$this->title="Votre galerie de chaussures";
			ob_start();
			include('research.php');
			$form=ob_get_clean();
			$this->content=$form;
			$this->content .= "<p>Cliquer sur une chaussure pour voir ses caractéristiques<p/>\n";
		}
		

	

	}

