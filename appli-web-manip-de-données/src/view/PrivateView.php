<?php

	class PrivateView extends View {

		protected $account;

		public function __construct(Account $newAccount, Router $newRouter, $newFeedBack){

			parent::__construct($newRouter, $newFeedBack);
			$this->account = $newAccount;
			

		}


		public function displayMenu(){

			$this->menu = array(

				'Accueil' => $this->router->getHomePage(),
				'Chaussures' => $this->router->getAllShoesPage(),
				'Nouvelle Chaussure' => $this->router->getShoeCreationURL(),
				'Déconnexion' => $this->router->getDonnectionPage(),
				'A propos' => $this->router->getInfoGroupe(),
				);

		return $this->menu;
	}



	public function makeHomePage() {

			$nomPersonneConnectee = $_SESSION['user'];
			$afficheNom = $nomPersonneConnectee->getNAME();
			$this->title ="Créer vos chaussures ";
			$contenu = "";
			$contenu.="<p>Bonjour <strong><em>$afficheNom</em></strong> </p>";
			$this->content = $contenu;
	}

	public function makeAproposPage() {

		$this->title ="A PROPOS ";
		$contenu = "<div class='apropos'>";
		$contenu.= "<h3> Vous êtes sur votre espace personnel </h3>";
		$contenu.= "Vous pourrez confectionner le type de chaussure que vous voulez.<br/>";
		$contenu.= "Faites votre précommande après confection. ";
		$contenu.= "</div>";
	

		$this->content = $contenu;
	}



}