<?php
	class Router{

		
		public function main(ShoeStorage $shoeStorage, AccountStorage $accountStorage)
		{

			session_start();

			if(!key_exists('feedback', $_SESSION))
			{
				$_SESSION['feedback'] ='';
			}

			//var_dump($accountStorage->readTable_utilisateur());
			//$authManager = new AuthenticationManager($accountStorage->readTable_utilisateur()/*$accountStorage->readTable_utilisateur()*/);
			//var_dump($authManager);
			

			if(key_exists('user', $_SESSION)){
				$connectedPerson=$_SESSION['user']->getLogin();
				$vue = new PrivateView($_SESSION['user'],$this,$_SESSION['feedback']);
			}else{
				$connectedPerson = 'visiteur';
				$vue = new View($this,$_SESSION['feedback']);
			}


			unset($_SESSION['feedback']);
			$controller = new Controller($vue, $shoeStorage,$accountStorage);
			$shoeID = key_exists('chaussure', $_GET)? $_GET['chaussure']: null;

			$nbShoes =  $controller->nbChaussure();

			$nShoesResearched ="";
			if(key_exists('marque',$_POST)){
				$nShoesResearched = $controller->showShoeSearchNumber($_POST['marque']);
			}
			

			//var_dump($_SESSION['user']->getLOGIN());
			

			//var_dump($shoeStorage->getShoeAuthorID($shoeID));
			//var_dump($accountStorage->getUserID($_SESSION['user']->getName()));
			//var_dump($shoeStorage->researchShoeByBrand('nike'));

			
			$droitsInternautes = array();

			if($connectedPerson != 'visiteur'){

				$droitsInternautes = array('nouveau','sauverNouveau','askDeletion','deletion', 'sauverModifs','modifier','dx','Accueil','id');
			} else 
			{
				$droitsInternautes = array('cx','Accueil','inscription');
			}


				if(key_exists('liste', $_GET) && $nbShoes == 0)
					{
						$vue->noShoeCReatedMessage();
					}
			
				
				elseif(key_exists('liste', $_GET) && $nbShoes > 0)
				{
					
					$controller->showList();

					if(key_exists('marque',$_POST) && $nShoesResearched == 0 )
					{
						$vue->noShoeFound();
					}
					elseif(key_exists('marque',$_POST) && $nShoesResearched > 0 )
					{
							$controller->showReseachList($_POST['marque']);
					}
							
				}
			
				elseif(key_exists('id',$_GET))
				{

					$controller->showInformation($_GET['id']);

					if(in_array('id', $droitsInternautes)){

						$controller->showInformation($_GET['id']);

					}else{
						$vue->makeNotFoundDEtails();
					}
					

				}elseif(key_exists('action', $_GET)) 
				{

					if($_GET['action']=='inscription'){
					$controller->newUser($_POST);
					}
				
					if($_GET['action'] == 'sauverInscription'){
			
						$controller->saveNewUser($_POST);
			
					}


					if($_GET['action'] == 'nouveau'){
						$controller->newShoe($_POST);

					}
				

					if($_GET['action'] == 'sauverNouveau'){
						if(!empty($_POST)){
							$controller->saveNewShoe($_POST);
						}
					}

					if($_GET['action'] == 'Accueil'){
						$vue->makeHomePage();
					}

					if($_GET['action'] == 'askDeletion'){
						if($shoeID === null){
							$vue->makeDebugPage($shoeID);
						}else{
							if($controller->verifyUser($shoeID,$_SESSION['user']->getLOGIN())){
								$controller->askShoeDeletion($shoeID);
							}else{
								$vue->noPermissionforDeletion();
							}
						}
					}

					if($_GET['action'] == 'deletion'){
						if($shoeID === null){
							$vue->makeDebugPage($shoeID);
						}else{
							$controller->deleteShoe($shoeID);
						}
					}

					if($_GET['action'] == 'Apropos'){
						$vue->makeAproposPage();
				}

				
					if($_GET['action'] == 'sauverModifs'){
						
							if($shoeID === null){
								$vue->makeDebugPage($shoeID);
							}else{
								if(!empty($_POST)){
									$controller->shoeUpdated($shoeID,$_POST);
								}
								
							}
						}
					

				
					if($_GET['action'] == 'modifier'){
						if($shoeID === null){
							$vue->makeDebugPage($shoeID);
						}else{

							if($controller->verifyUser($shoeID,$_SESSION['user']->getLOGIN())){

									$controller->isGoingToBeUpdated($shoeID);
							}else{
								$vue->noPermissionforUpdate();
							}
						}
					}

				
					if($_GET['action'] == 'cx'){
						$vue->makeLoginFormPage();

						if(key_exists('login', $_POST) && key_exists('lePass', $_POST)){
			
							$controller->connection($_POST['login'], $_POST['lePass']);
						}
					}
					
					if($_GET['action'] == 'dx'){

						$controller->disconnectAUSER();
					}

				}else
				{

					$vue->makeHomePage();
				}
			$vue->render();
				
			}



		public function getShoeURL($id){
			return "?id=".$id;
		}

		public function getShoeSaveURL(){
			return "Shoes.php?action=sauverNouveau";
		}

		public function getHomePage(){
			return 'Shoes.php?action=Accueil';
		}

		public function getShoeCreationURL(){
			return "Shoes.php?action=nouveau";
		}

		public function getAllShoesPage(){
			return 'Shoes.php?liste';
		}

		public function getInscription(){
			return 'Shoes.php?action=inscription';
		}


		public function getSaveInscription(){
			return 'Shoes.php?action=sauverInscription';
		}


		public function getConnectionPage(){
			return 'Shoes.php?action=cx';
		}

		public function getDonnectionPage(){
			return 'Shoes.php?action=dx';
		}


		public function getShoeAskDeletionURL($id){
			return 'Shoes.php?chaussure='.$id.'&action=askDeletion';
		}

		public function getShoeDeletion($id){
			return 'Shoes.php?chaussure='.$id.'&action=deletion';
		}

		public function getShoeUpdatePage($id) {
			return 'Shoes.php?chaussure='.$id.'&action=modifier';
		}

		public function getShoeUpdateSavedPage($id) {
			return 'Shoes.php?chaussure='.$id.'&action=sauverModifs';
		}
		
		
		public function getInfoGroupe(){
			return 'Shoes.php?action=Apropos';
		}

		public function POSTredirect($url, $feedback){
			$_SESSION['feedback'] = $feedback;
			header("Location:". $url, true,303);
		}


	}
