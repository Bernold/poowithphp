<?php
	class Controller{

		private $view;
		private $shoeStorage;
		private $accountStorage;
		private $authManager; 
		
	

		public function __construct(View $newView, ShoeStorage $newShoeStorage, AccountStorage $newAccountStorage){
			$this->view = $newView;
			$this->shoeStorage = $newShoeStorage;
			$this->accountStorage =  $newAccountStorage;
			$this->authManager = new AuthenticationManager($this->accountStorage);//$this->accountStorage est l'instance PDO construite dans ConnexionPDO.php. Cette classe Controller doit donc être appelée après l'appel du fichier ConnexionPDO.php dans shoes.php
		}

		public function nbChaussure(){
			return $this->shoeStorage->getShoesNumber();
		}
		
		// Affiche le détail d'une chaussure
		public function showInformation($id) {
			$aShoe=$this->shoeStorage->read($id);

			if($aShoe !== null){
				$this->view->makeShoePage($aShoe,$id);
			}else{
				$this->view->makeUnknownShoePage();
			}
		}

		// Renvoi la liste des chaussures créées
		public function showList(){
			return $this->view->makeShoeListPage($this->shoeStorage->readAll());
		}

		// Renvoi la liste la liste des chaussures recherchés
		public function showReseachList($marque){
			$listeChaussures = $this->shoeStorage->researchShoeByBrand($marque);

				return $this->view->makeShoeListPageByResearch($listeChaussures);
		}

		// Renvoi le nombre de chaussures recherchés
		public function showShoeSearchNumber($marque){
			return $this->shoeStorage->getShoesSearchNumber($marque);
		}




		// Permet la création d'une chaussure
		public function newShoe($aShoe){
			
			if(key_exists('currentNewShoe',$_SESSION)){
				$this->view->makeShoeCreationPage($_SESSION['currentNewShoe']);
			}
			

			if(!key_exists('currentNewShoe',$_SESSION)){
				$shoe = new ShoeBuilder($aShoe,$_SESSION['user']);
				$this->view->makeShoeCreationPage($shoe);
			}
		}


		// Permet la sauvegarde d'une chaussure créée
		public function saveNewShoe(array $data){
			$author = $_SESSION['user'];
			$aShoeBuilder = new ShoeBuilder($data,$author);

			if($aShoeBuilder->isValid()){
				$aShoeBuilt = $aShoeBuilder->createShoe();
				$shoeID = $this->shoeStorage->create($aShoeBuilt,$author);
				$this->view->displayShoeCreationSuccess($shoeID);
				
			}else{
				$_SESSION['currentNewShoe']=$aShoeBuilder; // Garde en mémoire la chaussure tronquée
				//$this->view->displayShoeCreationFailure();
				$this->view->makeShoeCreationPage($aShoeBuilder);
			}
		}


		// Demande la confirmation de la suppression de la chaussure
		public function askShoeDeletion($id){
			$theShoe = $this->shoeStorage->read($id);
			if($theShoe !== null){
				$this->view->makeShoeDeletionPage($id,$theShoe);
				return true;
			}else{
				$this->view->makeUnknownShoePage();
				return false;
			}
		}

		// Permet la suppression d'une chaussure
		public function deleteShoe($id){
			if($this->askShoeDeletion($id)){
				$this->shoeStorage->delete($id);
				$this->view->displaySuppressionSuccess();
			}
		}


			// Prépare une page pour la modification d'une nouvelle chaussure 
			public function isGoingToBeUpdated($shoeID)
			{
				$shoe=$this->shoeStorage->read($shoeID); // La chaussure est obtenue grâce à son  id "$shoeID"

				
				if(key_exists('modifiedShoes',$_SESSION) )
				{
						$this->view->makeShoeUpdatePage($shoeID, $_SESSION['modifiedShoes']);
				}
	
				if(!key_exists('modifiedShoes',$_SESSION))
				{
					//Extraction des données modifiables
					$donneesRec = ShoeBuilder::buildFromShoe($shoe);
					//Préparation de la page du formulaire
					$this->view->makeShoeUpdatePage($shoeID,$donneesRec);
				}
			}


		// Modifie une chaussure et la stocke en base de données avec son id initial
		public function shoeUpdated($idOldObject, array $newObject){
		
			$anotherShoeBuilder = new ShoeBuilder($newObject);
			$oldShoe=$this->shoeStorage->read($idOldObject);

				if($anotherShoeBuilder->isValid()){
					
					$anotherShoeBuilder->updateShoe($oldShoe); // modification
					$this->shoeStorage->storeUpdatedShoe($idOldObject,$oldShoe); // stockage

					$res = $this->shoeStorage->getShoesNumber();
				
					$this->view->displayShoeUpdatingSuccess($idOldObject);
					unset($_SESSION['modifiedShoes']);
					
				}else{
					
					$_SESSION['modifiedShoes'] = $anotherShoeBuilder; // $anotherShoeBuilder ici, c'est la chaussure dont la modification est tronquée
					$this->view->makeShoeUpdatePage($idOldObject, $_SESSION['modifiedShoes']);
					
					/*
					var_dump($_SESSION['modifiedShoes']);
					var_dump($idOldObject);
					//echo "bugg"; 
					*/
				}
			
		}

		
		// Reçoit l'instance d'un utilisateur
		public function newUser($anArray){
			$user = new AccountBuilder($anArray);
			$this->view->makeInscription($user);
		}


		// Sauvegarde le nouvel utilisateur créé si l'instance qui le représente est valide
		public function saveNewUser(array $data){
			$utilisateur = new AccountBuilder($data);
			if($utilisateur->isValid()){

				$userBuilt = $utilisateur->createAccount();

				if($this->accountStorage->ifLoginAlreadyExists($userBuilt->getLOGIN()))
				{
					$utilisateurID = $this->accountStorage->createAUser_In_Table_utilisateur($userBuilt);
					$this->view->displayCompteCreationSuccess($utilisateurID);
				}else{
					$this->view->makeInscription_2($utilisateur);
				}

			}else{
				$this->view->makeInscription($utilisateur);
			}
		}


		// Permet à un utilisateur de se connecter
		public function connection($login,$password){
			if($this->authManager->connectUser($login,$password)){

				$this->view->displayConnexionSucess();
				}else{
					$this->view->displayConnexionFailure();
				}
			}
			
		// Permet à un utilisateur de se déconnecter
		public function disconnectAUSER(){
			$this->authManager->disconnectUser();
			$this->view->deconSucess();
		}


		/*
			Cette méthode ci-dessous permet de restreindre la modification 
			et la supression d'une chaussure qu'à la personne qui 
			l'a créé
		*/
		public function verifyUser($idChaussure,$userPseudo){
			if($this->authManager->isUserConnected()){

				if($this->shoeStorage->getShoeAuthorID($idChaussure) == $this->accountStorage->getUserID($userPseudo)){
					return true;
				}else{
					return false;
				}
			}
		}












}