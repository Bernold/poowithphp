<?php
/*
PLugin Name: BernoldPlugin
Description: Une introduction au développement de plugin
Version: 1.0.1
Author: Bernold
Author URI: https://gitlab.com/Bernold/poowithphp
Domain Path: /languages
Text Domain: my-plugin
*/


/*
 * Si la variable ABSPATH n'est pas définie,
 * le reste du script de ce fichier ne sera pas exécuté.
 * Autrement dit, wordpress ne fonctionnera pas si ABSPATH
 * n'est pas définie
 */
if(!defined('ABSPATH')){
    exit;
}

define('BERNOLD_PLUGIN_ADMIN_PATH',plugin_dir_path(__FILE__). 'admin' ); // Permet à l'appli de reconnaître le contenu du dossier "admin"

include plugin_dir_path(__FILE__).'includes/default.php';

require_once plugin_dir_path(__FILE__).'includes/class-plugin-base.php'; // Importation d'abord de la classe mère
require_once plugin_dir_path(__FILE__).'includes/class-etendre-pluginBase.php';        // Ensuite de la classe fille
require_once plugin_dir_path(__FILE__).'includes/class-active_deactive-plugin.php';

/*
 * Signaux d'activation et de
 * désactivation de plugin
 */
register_activation_hook(__FILE__, 'plugin_activation');      // Action d'activation
register_deactivation_hook(__FILE__,'plugin_deactivation');   // Action de désactivation


function plugin_activation(){
    // Exécution du code d'activation
    // Installation des tables dans la base de données
    // Instanciation des classes
    $activator = new My_PluginActivatorAndDeactivator();
    $activator->odi_activate_plugin();
}


function plugin_deactivation(){
    // Exécution  du code de désactivation : clean up de l'extension
    // Suppression des tables de l'extension
    // Suppression les fichiers appartenant à l'extension
    $deactivator = new My_PluginActivatorAndDeactivator();
    $deactivator->odi_deactivate_plugin();
}

add_action('plugins_loaded', 'the_main_checks_plugin_version'); // add_action charge mon plugin à chaque fois qu'il sera sollicité

function the_main_checks_plugin_version(){
    $versionChecker = new My_PluginActivatorAndDeactivator();
    $versionChecker->check_plugin_version();
}

$publicVAR = new PluginBaseSon(); // Permet l'accès à la partie publique de l'appli

