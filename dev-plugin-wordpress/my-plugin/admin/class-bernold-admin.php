<?php

if(!defined('ABSPATH')){
    exit;
}

class Bernold_Admin extends PluginBase
{

    public function __construct(){

        $this->load_dependencies();     // Charge en premier les fichiers necessaires avant le traitement des données
        $this->initialize_hooks();
    }

    protected function initialize_hooks()
    {
        // Création du menu de mon plugin. Ce qui pourra permettre de le charger
        add_action('admin_menu', array($this, 'register_main_page'));
    }

    public function register_main_page(){
        //1. Menu principal
        add_menu_page(
            'Bernold-Tech-admin-page',
            'Bernold-Tech',
            'manage_options',
            MY_PLUGIN_ATTRIBUTE,
            array($this, 'handle_main_page'),
            'dashicons-admin-plugins',
            30
        );
    }

    public function handle_main_page(){
        echo '<h2> Hello to you <h2>';
    }

    protected function load_dependencies()
    {
        // TODO: Implement load_dependencies() method.
    }
}