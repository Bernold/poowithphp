<?php

if(! defined('ABSPATH')){
    exit;
}

/**
 * BERNOLDPLUGIN_DB_VERSIONNING  est une variable qui contiendra à chaque fois une nouvelle version des tables constituant le plugin créé
 */

define('MY_PLUGIN_ATTRIBUTE_VALUE','1.0.1');    // Valeur de l'attribut de la version du plugin
define('MY_PLUGIN_ATTRIBUTE', 'bernold');       // Attribut de la version du plugin






