<?php

if(! defined('ABSPATH')){
    exit;
}

class My_PluginActivatorAndDeactivator {
    private $the_db_version_attribute;

    public function __construct(){
        $this->the_db_version_attribute = MY_PLUGIN_ATTRIBUTE . '_db_version';
    }

    /**
     * Initialisations
     */
    public function odi_activate_plugin(){
        // Création des tables dens la base de données de wordpress
        $this->initialize_tables();
    }

    /**
     * Cleanup
     */
    public function odi_deactivate_plugin(){
        // Suppression de toutes les tables créées lors de l'activation de l'extension
        global $wpdb;
        $tables = array(
            $wpdb->prefix . 'myPluginSettings',
            $wpdb->prefix . 'myPluginInfos'
        );
        foreach ($tables as $table) {
            $wpdb->query("DROP TABLE IF EXISTS {$table}");
        }
        // Suppression de la version de l'extension précedement activée
        delete_option($this->the_db_version_attribute);
    }

    // Cette méthode permet de vérifier si la version de mon plugin a besoin d'être mis à jour une fois qu'il est chargé sur le tableau de bord wordpress
    public function check_plugin_version(){
        $new_db_version_value = MY_PLUGIN_ATTRIBUTE_VALUE; // Version courante de la bd wordpress. Elle peut être originale ou non. Cela dépend de l'existence d'une imputation préalable de plugin
        $activated_plugin_db_version_value = get_site_option($this->the_db_version_attribute); // get_site_option() renvoie la valeur de l'attribut de l'extension déjà activée

        if($new_db_version_value != $activated_plugin_db_version_value)             // Si la version du plugin activé est différente de celle précédemment activé, faudra créer de nouvelles tables
        {
            // Installer les nouvelles tables
            $this->initialize_tables();
            update_option($this->the_db_version_attribute, $new_db_version_value);  // Edition de la nouvelle bd. Son attribut + sa valeur
        }
    }

    public function initialize_tables() {
        global  $wpdb;
        $charset_collate = $wpdb->get_charset_collate();                //  Prise en compte de la casse et des caractères accentués qui seront stockés dans la base
        $pluginTables = $this->get_tables_created($charset_collate);

        $the_db_version_attribute_value = MY_PLUGIN_ATTRIBUTE_VALUE;    // $db_version contient la valeur de l'attribut du plugin

        require_once (ABSPATH. 'wp-admin/includes/upgrade.php');       // "upgrade.php" est un fichier wordpress qui permet de mettre à jour sa bd si nécessaire

        foreach ($pluginTables as $table) {
            try {
                $tableName = key($table);                              // "$tableName" récupère le nom(qui constitue une clé de tableau associatif) de chaque table contenu dans $pluginArrays
                $tableStructure = $table[$tableName];                  // "$tableStructure" est la structure(qui constitue une valeur de tableau associatif) de chaque table
                $query = "CREATE TABLE $tableName $tableStructure";    // "$query" reçoit la formulation de la requête de création de table
                /**
                 *  dbDelta permet d'ajouter une ou plusieurs tables dans la bd de wordpress lors de l'activation d'un plugin
                 *  ou lors de l'installation d'un thème
                 */
                dbDelta($query, true);
            }catch (Exception $error){

            }
        }
        // SAUVEGARDE DE LA BASE DE DONNÉES À CHAQUE FOIS QU'UNE OU PLUSIEURS TABLES LUI SONT IMPUTÉES(SAUVEGARDE DE LA SA NOUVELLE VERSION DU PLUGIN)
        add_option($this->the_db_version_attribute, $the_db_version_attribute_value); // Update de la version de la base de données avec la méthode "add_option"

    }

    public function get_tables_created($charset_collate){
        global $wpdb;
        $appTables = array(
            /** "description" est une nouvelle propriété qui plus tard a été ajouté à la table myPluginSettings.
             * Il faudra donc changer la version de notre plugin dans le fichier 'default.php'
             */
            array($wpdb->prefix.'myPluginSettings' =>
                "( 
                    id mediumint(9) NOT NULL AUTO_INCREMENT,
                    name VARCHAR(50) NOT NULL,   
                    createdAt datetime NOT NULL,
                    deleteAt datetime NULL,
                    description VARCHAR(255) NULL,
                    PRIMARY KEY (id)
                )$charset_collate;"             // Préparation à l'encodage par défaut de wordpress
                ),

            array($wpdb->prefix.'myPluginInfos' =>
                "(
                    id mediumint(9) NOT NULL AUTO_INCREMENT,
                    app_name VARCHAR(50) NOT NULL,
                    createdAt datetime NOT NULL,
                    deleteAt datetime NULL,
                    PRIMARY KEY (id)
                )$charset_collate;"
                )
        );
        return $appTables;
    }
}