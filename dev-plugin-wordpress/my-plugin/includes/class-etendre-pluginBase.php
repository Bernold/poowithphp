<?php

/**
 * class NKT: Représente l'extension lui-même
 * @since 1.0.1
 * @author Bernold-tech | Bernold d'ALMEIDA
 */

// CETTE CLASSE DISPACTHE LES FONCTIONNALITÉS LIÉES A  L'ADMIN ET AU PUBLIC

class PluginBaseSon extends PluginBase {

    protected $admin_plugin;


    public function __construct()
    {
        $this->load_dependencies(); // Charger en premier les fichiers necessaires avant le traitement des données
        $this->initialize_hooks();
    }

    /**
     * Initialiser les actions et les filtres
     */
    protected function initialize_hooks()
    {
        // TODO: Implement initialize_hooks() method.
    }


    /**
     * Charger les fichiers nécessaires pour le bon fonctionnement de l'extension
     */
    protected function load_dependencies()
    {
        // "is_admin()" est une méthode wordpress
        if(is_admin()) {
            require_once(BERNOLD_PLUGIN_ADMIN_PATH.'/class-bernold-admin.php');
            $this->admin_plugin = new Bernold_Admin();
        }

    }
}