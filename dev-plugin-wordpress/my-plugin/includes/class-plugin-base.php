<?php

abstract class PluginBase {
    /**
     * Initialise les actions et les filtres
     */
    protected abstract function initialize_hooks();


    /**
     * Charge les fichiers nécessaires pour le bon fonctionnement de l'extension
     */
    protected  abstract function load_dependencies();


}